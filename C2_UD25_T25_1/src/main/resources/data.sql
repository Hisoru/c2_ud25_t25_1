DROP TABLE IF EXISTS `articulos`;
DROP TABLE IF EXISTS `fabricantes`;


CREATE TABLE `fabricantes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `articulos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `precio` int DEFAULT NULL,
  `fab_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `articulos_fk` FOREIGN KEY (`fab_id`) REFERENCES `fabricantes` (`id`)
);


insert into fabricantes (nombre)values('Naviator');
insert into fabricantes (nombre)values('Destiner');
insert into fabricantes (nombre)values('Crewhale');
insert into fabricantes (nombre)values('Wavess');
insert into fabricantes (nombre)values('Sailjet');

insert into articulos (nombre, precio,fab_id) values ('Tent',39,1);
insert into articulos (nombre, precio,fab_id) values ('Sleeping bag',49,2);
insert into articulos (nombre, precio,fab_id) values ('Camp chair',67,3);
insert into articulos (nombre, precio,fab_id) values ('Camp table',28,4);
insert into articulos (nombre, precio,fab_id) values ('Lantern',50,5);

